Yii2 Voodoo Mobile
===================

Installation
------------
The preferred way to install this extensions is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require "voodoo-mobile/yii2vm" "*"
```
or add

```json
"voodoo-rocks/yii2vm" : "*"
```

to the require section of your application's `composer.json` file.

[![Build status](https://badge.buildkite.com/09fba50ff2bdc61f9468455938b03f30e13bf9edd5365c2248.svg)]
(https://buildkite.com/voodoo-rocks/yii2vm)
