var gulp = require('gulp');
var concat = require('gulp-concat');
var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');

var paths = 'src/assets';

gulp.task('styles', function() {
    gulp.src(paths + '/distribution/less/*.less')
        .pipe(less())
        .pipe(cssmin())
        .pipe(rename('yii2vm.min.css'))
        .pipe(gulp.dest(paths + '/css'))
        .pipe(livereload());
});

gulp.task('scripts', function() {
    gulp.src(paths + '/distribution/js/*.js')
        .pipe(concat('yii2vm.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths + '/js'))
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(paths + '/distribution/less/*.less', ['styles']);
    gulp.watch(paths + '/distribution/js/*.js', ['scripts']);
});

gulp.task('default', ['styles', 'scripts', 'watch']);
