<?php

namespace yii2vm\api;

use Yii;
use yii\base\Exception;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24/03/15
 * Time: 00:30
 */
class Module extends \yii\base\Module
{
    public $controllerMap = [
        'doc'       => 'yii2vm\api\controllers\DocController',
        'android'   => 'yii2vm\api\controllers\AndroidController',
        'tests'     => 'yii2vm\api\controllers\TestsController',
        'overview' => 'yii2vm\api\controllers\OverviewController',
    ];

    public function init()
    {
        parent::init();

        if (!YII_DEBUG) {
            $this->controllerMap = [];
        }

        Yii::setAlias('@yii2vm', __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..');
        Yii::setAlias('@api', __DIR__ . DIRECTORY_SEPARATOR);

        if (!Yii::$app->has('api')) {
            throw new Exception('API component is missing in the config file. Please add a component that is object of @yii2vm\api\components\Api');
        }

        if (Yii::$app->api->enableDocs) {
            $this->controllerMap['doc'] = 'yii2vm\api\controllers\DocController';
        }

        Yii::$app->response->formatters = array_merge(
            Yii::$app->response->formatters,
            [
                Response::FORMAT_JSON => '\yii2vm\api\components\JsonResponseFormatter',
            ]
        );
    }
}