<?php
namespace yii2vm\api\controllers;

use yii\web\Controller;

class OverviewController extends Controller
{
    public $layout = '@api/views/layouts/main';

    public function actionIndex()
    {
        return $this->render('@api/views/overview/index');
    }
}