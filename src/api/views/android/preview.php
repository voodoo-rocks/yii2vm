<?php

use yii\widgets\ActiveForm;

?>

<div class="page-header">
    <h1>
        Android SDK
    </h1>
</div>

<?php $form = ActiveForm::begin([]) ?>

<?= \app\components\Html::submitButton('Download', ['class' => 'btn btn-primary']) ?>

<?php $form->end() ?>
