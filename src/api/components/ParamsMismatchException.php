<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 31/03/15
 * Time: 10:55
 */

namespace yii2vm\api\components;

class ParamsMismatchException extends \Exception
{

    private $difference = null;

    public function __construct($difference)
    {
        parent::__construct();

        $this->difference = $difference;
    }
}