<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03/04/15
 * Time: 06:05
 */

namespace yii2vm\api\components;

use Yii;
use yii\base\ActionFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

class ApiCheckerFilter extends ActionFilter
{
    public function beforeAction($action)
    {
        if (Yii::$app->api->requiresKey) {

            /** @var \ArrayObject $this->owner->request */
            $apiKey = ArrayHelper::getValue($this->owner->request, 'key');
            if (!$apiKey || !in_array($apiKey, \Yii::$app->api->keys)) {

                throw new BadRequestHttpException('Missing or invalid API key');
            }
        }

        return true;
    }
}