<?php

namespace yii2vm\api\components\auth;

use yii\filters\auth\AuthMethod;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\UnauthorizedHttpException;
use yii2vm\components\ArrayObject;

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03/04/15
 * Time: 06:13
 */
class TokenAuth extends AuthMethod
{
    public $accessTokenPath = ['token', 'hash'];

    /**
     * Authenticates the current user.
     *
     * @param \yii\web\User     $user
     * @param \yii\web\Request  $request
     * @param \yii\web\Response $response
     *
     * @return \yii\web\IdentityInterface the authenticated user identity. If authentication information is not provided,
     *                                    null will be returned.
     * @throws \yii\web\UnauthorizedHttpException if authentication information is provided but is invalid.
     */
    public function authenticate($user, $request, $response)
    {
        $identity = true;
        $json     = new ArrayObject(Json::decode($request->rawBody));

        /** @var ArrayObject $request */
        /** @noinspection PhpUndefinedFieldInspection */
        $request = $json->request;
        if (is_a($request, 'yii2vm\components\ArrayObject')
            && $request->has(ArrayHelper::getValue($this->accessTokenPath, 0))
        ) {
            $token = $request;
            foreach ($this->accessTokenPath as $part) {
                $token = $token->{$part};
            }

            $identity = $user->loginByAccessToken($token);

            if (!$identity) {
                throw new UnauthorizedHttpException('Incorrect or expired token provided');
            }
        } else {
            if (!$user->isGuest) {
                $user->logout();
            }
        }

        return $identity;
    }
}