<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/04/15
 * Time: 13:54
 */

namespace yii2vm\api\components;

use yii\helpers\Json;

class JsonResponseFormatter extends \yii\web\JsonResponseFormatter
{
    protected function formatJson($response)
    {
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        if ($response->data !== null) {
            $response->content = Json::encode($response->data, JSON_PRETTY_PRINT);
        }
    }
}