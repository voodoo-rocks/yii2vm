<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 26/03/15
 * Time: 12:37
 */

namespace yii2vm\api\components;

use yii\base\Exception;

class VerboseException extends Exception
{
    public $template;

    public function __construct($template)
    {
        parent::__construct();
        $this->template = $template;
    }
}