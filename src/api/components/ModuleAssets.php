<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 24/03/15
 * Time: 03:51
 */

namespace yii2vm\api\components;

use yii\web\AssetBundle;

class ModuleAssets extends AssetBundle
{
    public $sourcePath = '@api/assets';

    public $js = [
        'js/api.js'
    ];

    public $css = [
        'css/api.css'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}