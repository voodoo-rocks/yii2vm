<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 01/04/15
 * Time: 09:06
 */

namespace yii2vm\api\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class Api
 * @package yii2vm\api\components
 */
class Api extends Component
{
    /**
     * @var string
     */
    public $version = '1.0';

    /**
     * @var bool
     */
    public $enableDocs = true;

    /**
     * @var bool
     */
    public $enableProfiling = false;

    /**
     * @var bool
     */
    public $requiresKey = false;

    /**
     * @var array
     */
    public $keys = [];

    /**
     * @return mixed
     */
    public function getRandomKey()
    {
        return ArrayHelper::getValue(\Yii::$app->api->keys, array_rand(\Yii::$app->api->keys));
    }
}