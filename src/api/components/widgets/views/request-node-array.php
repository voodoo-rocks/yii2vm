<?php

use yii2vm\api\components\widgets\RequestNode;

?>

<div class="toggle-properties open">
    <?= $key ?> [
</div>

<ul class="list-json">
    <?= RequestNode::widget([
        'node' => $array
    ]); ?>
</ul>
]