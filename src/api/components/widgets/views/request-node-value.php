<?= $key ?> <?= dosamigos\editable\Editable::widget([
    'type'          => 'text',
    'name'          => uniqid(),
    'value'         => $value,
    'url'           => 'editable',
    'clientOptions' => [
        'pk'        => uniqid(),
        'emptytext' => 'null',
        'display' => new \yii\web\JsExpression('function(value, sourceData)  {
            if (isNaN(value)) {
                value = \'"\' + value + \'"\';
            }
            $(this).html(value);
        }')
    ]
]) ?>