<?php

use yii2vm\api\components\widgets\RequestNode;

/**
 * @var $key
 * @var $value
 */

?>
<div class="toggle-properties open">
    <?= $key ?> {
</div>
<ul class="list-json">
    <?= RequestNode::widget(['node' => $value]) ?>
</ul>
}