<?php

namespace yii2vm\config;

use Yii;
use yii\base\Exception;

/**
 * @deprecated
 */
class FileDetector extends Detector
{
    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function detected()
    {
        $filename = isset($this->params->filename) ? $this->params->filename : $this->name;

        if (!$filename) {
            throw new Exception('Filename is not set up properly');
        }
        $exists = file_exists(Yii::$app->basePath . DIRECTORY_SEPARATOR . $filename);

        return $exists;
    }
}