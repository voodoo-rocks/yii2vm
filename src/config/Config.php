<?php

namespace yii2vm\config;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii2vm\components\ObjectUtils;

/**
 * @deprecated
 */
class Config extends Component
{
    /**
     *
     */
    const DEFAULT_DETECTOR = 'yii2vm\config\FileDetector';

    /**
     * @var Detector Current detector which will activate a config
     */
    public $detector = null;

    /**
     * @var array Option changes which has to be applied to the Yii->$app options if this configuration is active
     */
    public $options = [];

    /**
     * @var bool Identifies if the config is default
     */
    public $default = null;

    /**
     * @var string|null Name of config. For most detectors it will be used as a base information about which config to
     *      activate
     */
    private $name = null;

    /**
     * Constructs a base object with passed params. Params should contain options or they have to be applied later
     *
     * @param array $name   Name of configuration
     * @param array $params Parameters of object. Parameters will be passed to the base constructor (see Component)
     */
    public function __construct($name, $params)
    {
        parent::__construct($params);

        $this->name = $name;
        $params     = ObjectUtils::fromArray($params);

        $this->options = @$params->options ? $params->options : [];
        $detectorClass = null;

        if (isset($params->detector)) {
            $detectorOptions = $params->detector;

            if (isset($detectorOptions->class)) {
                $detectorClass = $detectorOptions->class;
            }
        }

        if (!$detectorClass) {
            $detectorClass = self::DEFAULT_DETECTOR;
        }

        $this->detector = new $detectorClass($name, $this->options);
    }

    /**
     * @return bool Returns if the config is active
     */
    public function isActive()
    {
        return $this->detector->detected();
    }

    /**
     * Applies all options to Yii::$app->params
     * @return bool
     */
    public function run()
    {
        if ($this->options) {
            foreach (ArrayHelper::toArray($this->options) as $key => $value) {
                $this->setValue(\Yii::$app, $key, $value);
            }
        }

        return true;
    }

    /**
     * Sets the value for the parameter by the key for the passed Component.
     *
     * @param \yii\base\Component $component
     * @param                     $key
     * @param                     $value
     */
    public function setValue(Component $component, $key, $value)
    {
        if (is_array($value) && is_a($component->{$key}, Component::className())) {
            foreach ($value as $subKey => $subValue) {
                $this->setValue($component->{$key}, $subKey, $subValue);
            }
        } else {
            $component->{$key} = $value;
        }
    }
}