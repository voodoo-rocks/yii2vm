<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 27/01/15
 * Time: 14:29
 */

namespace yii2vm\config;

use yii\base\Component;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class Configurator
 * @package yii2vm\config
 *
 * @deprecated
 */
class Configurator extends Component
{
    /**
     * @var array
     */
    public $configs = [];

    /**
     * @throws \yii\base\Exception
     */
    public function init()
    {
        if (!$this->configs || !count($this->configs)) {
            throw new Exception('No configurations detected. Please specify one or more');
        }

        // Find the default config
        $default = array_filter($this->configs, function ($config) {
            return is_array($config) && ArrayHelper::getValue($config, 'default', false);
        });

        // Check there is only one default config
        if (is_array($default) && count($default) > 1) {
            throw new Exception('There are more than one default config');
        }

        // Apply the detected config first otherwise apply the default one if exists
        if (!$this->loop($this->configs) && $default) {
            $names = array_keys($default);
            $name = array_pop($names);
            $instance = new Config($name, $default[$name]);
            $instance->run();
        }

        return false;
    }

    private function loop($configs)
    {
        // Loop through configurations and find active
        foreach ($configs as $name => $config) {
            /** @var Config $instance */
            $instance = new Config($name, $config);

            // Find an active config and launch it
            if ($instance->isActive()) {
                return $instance->run();
            }
        }

        return false;
    }
}