<?php

namespace yii2vm\config;

use yii\base\Component;
use yii2vm\components\ObjectUtils;

/**
 * @deprecated
 */
abstract class Detector extends Component
{
    /**
     * @var mixed|null
     */
    protected $params;
    /**
     * @var array|null
     */
    protected $name = null;

    /**
     * Creates an object
     *
     * @param array $name
     * @param array $params
     */
    public function __construct($name, $params = [])
    {
        $this->name   = $name;
        $this->params = $params != null ? ObjectUtils::fromArray($params) : null;
    }

    /**
     * @return mixed Abstract class that informs a configurator if this detector has been triggered
     */
    public abstract function detected();
}