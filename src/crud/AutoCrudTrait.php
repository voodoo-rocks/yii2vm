<?php

namespace yii2vm\crud;

use dosamigos\editable\EditableAction;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class AutoCrudTrait
 * @package yii2vm\crud
 */
trait AutoCrudTrait
{
    /**
     * @return string
     */
    abstract protected function getModelClass();

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => call_user_func([$this->getModelClass(), 'find'])
        ]);

        /** @var Controller|AutoCrudTrait $this */

        return $this->render($this->pickView('index'), [
            'dataProvider' => $dataProvider,
            'columns'      => $this->guessColumns($dataProvider)
        ]);
    }

    public function actionCrudEditable()
    {
        $action = Yii::createObject(EditableAction::className(), [
            'crud-editable',
            $this,
            [
                'modelClass' => $this->getModelClass()
            ]
        ]);

        return $action->run();
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        /** @var Controller|AutoCrudTrait $this */
        /** @var ActiveRecord $model */
        $model = \Yii::createObject($this->getModelClass());

        return $this->edit($model);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        return $this->edit($this->findModel($id));
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        /** @var Controller|AutoCrudTrait $this */
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param ActiveRecord $model
     * @param array        $viewParams
     *
     * @return \yii\web\Response
     */
    protected function edit($model, $viewParams = [])
    {
        /** @var Controller|AutoCrudTrait $this */
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render($this->pickView('edit'), $viewParams + [
                'model' => $model
            ]
        );
    }

    /**
     * @param $id
     *
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $class = $this->getModelClass();

        /** @var ActiveRecord $model */
        $model = call_user_func([$class, 'findOne'], [$id]);
        if (!$model) {
            throw new NotFoundHttpException('Model not found');
        }

        return $model;
    }

    /**
     * @param $view
     *
     * @return string
     */
    private function pickView($view)
    {
        /** @var Controller|AutoCrudTrait $this */
        $extension = $this->getView()->defaultExtension;
        $default   = $this->getViewPath() . DIRECTORY_SEPARATOR . $view . '.' . $extension;

        if (file_exists($default)) {
            return $view;
        }

        Yii::setAlias('@trait', __DIR__);

        return '@trait/views/' . $view;
    }

    /**
     * @param BaseDataProvider $dataProvider
     *
     * @return array
     */
    private function guessColumns($dataProvider)
    {
        $columns = [];

        $models = $dataProvider->getModels();
        $model  = reset($models);

        if (is_array($model) || is_object($model)) {
            foreach ($model as $name => $value) {
                $columns[] = $name;
            }
        }

        return $columns;
    }
}