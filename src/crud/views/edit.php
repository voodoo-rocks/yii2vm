<?php

/** @var ActiveRecord $model */

use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

echo \yii\grid\GridView::widget([
    'dataProvider' => new ArrayDataProvider([
        'allModels' => ArrayHelper::getColumn(array_keys($model->attributes),
            function ($key) use ($model) {
                return [
                    'attribute' => $key,
                    'value'     => $model->getAttribute($key),
                ];
            }
        )
    ]),
    'columns'      => [
        'attribute',
        [
            'attribute' => 'value',
            'format'    => 'raw',
            'value'     => function ($pair) use ($model) {
                $attribute = ArrayHelper::getValue($pair, 'attribute');
                if ($attribute) {
                    $pk = call_user_func([$model->className(), 'primaryKey']);
                    if (in_array($attribute, $pk)) {
                        return ArrayHelper::getValue($pair, 'value');
                    }
                }

                return \dosamigos\editable\Editable::widget([
                    'url'           => ['crud-editable'],
                    'clientOptions' => [
                        'value' => ArrayHelper::getValue($pair, 'value'),
                        'pk'    => $model->primaryKey
                    ],
                    'name'          => $attribute
                ]);
            }

        ]
    ]
]);