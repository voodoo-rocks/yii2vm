<?php

use yii\data\BaseDataProvider;
use yii\helpers\Inflector;
use yii\helpers\Url;

/** @var BaseDataProvider $dataProvider */

?>

<div class="row">
    <div class="col-xs-12">
        <h1 class="pull-left">
            <?= Inflector::titleize(Inflector::pluralize(Yii::$app->controller->id)) ?>
        </h1>

            <a href="<?= Url::to(['create'])?>" class="btn btn-default pull-right">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php

        $columns[] = [
            'class'    => \yii\grid\ActionColumn::className(),
            'template' => '{update} {delete}',
            'options'  => [
                'class' => 'col-xs-1'
            ]
        ];

        $gridView = \yii\grid\GridView::begin([
            'dataProvider' => $dataProvider,
            'columns'      => $columns
        ]);

        $gridView->end(); ?>
    </div>
</div>