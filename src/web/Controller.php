<?php
namespace yii2vm\web;

use yii\helpers\Inflector;

/**
 * Class Controller
 * @package yii2vm\web
 *
 * Use this class as a parent class for all web controllers
 */
class Controller extends \yii\web\Controller
{
    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->view->title =
            Inflector::camel2words(
                $action->id == $action->controller->defaultAction ?
                    $action->controller->id : $action->id);

        return parent::beforeAction($action);
    }

}