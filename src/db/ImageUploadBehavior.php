<?php
namespace yii2vm\db;

use yii\base\UnknownMethodException;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\UploadedFile;
use yii2vm\media\upload\ImageUpload;

/**
 * Class ImageUploadBehavior
 * @package yii2vm\db
 *
 * @deprecated
 */
class ImageUploadBehavior extends AttributeBehavior
{
    /**
     *
     */
    const THUMBNAIL_URL_METHOD = 'thumbnail';

    /**
     * @var
     */
    public $imageAttribute;

    /**
     * @var bool
     */
    public $usePlaceholders = true;

    public $placeholderFile = null;

    public $customPlaceholderAlias = null;

    /**
     * @var array|null
     */
    public $thumbnails = [];

    /**
     * @var array|int|null
     */
    public $resize = null;

    public $absolutePath = false;
    /**
     * @var bool
     */
    private $inProcess = false;

    /**
     *
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                ActiveRecord::EVENT_AFTER_INSERT => $this->imageAttribute,
                ActiveRecord::EVENT_AFTER_UPDATE => $this->imageAttribute
            ];
        }
    }

    /**
     * @param      $attribute
     *
     * @return bool|string
     */
    public function url($attribute)
    {
        if ($this->owner->$attribute) {
            $local = Url::to('@webroot/' . $this->owner->$attribute);

            if (file_exists($local)) {
                return Url::to('@web/' . $this->owner->$attribute, true);
            }
        }

        if ($this->usePlaceholders) {
            if ($this->placeholderFile) {
                return Url::to('@web/'.$this->placeholderFile);
            }
            return 'http://placehold.it/500x500?text=No+Image';
        }

        return null;
    }

    /**
     * @param $attribute
     * @param $size
     *
     * @return bool|string
     */
    public function thumbnail($attribute, $size)
    {
        $filename = $this->owner->$attribute;

        if (!$filename && !$this->usePlaceholders) {
            return null;
        }

        list($width, $height) = $this->getDimension($size);

        if (!file_exists($filename)) {
            if ($this->placeholderFile) {

                $filename = \Yii::getAlias($this->placeholderFile);
                $thumbFilename = ImageUpload::thumbnailFilename($filename, $width, $height);
                if (!file_exists($thumbFilename)) {
                    Image::thumbnail($filename, $width, $height)->save($thumbFilename);
                }

                return Url::to('@web/' . $thumbFilename);
            }
            return sprintf('http://placehold.it/%dx%d', $width, $height);
        }

        $thumbFilename = ImageUpload::thumbnailFilename($filename, $width, $height);

        if (!file_exists($thumbFilename)) {
            Image::thumbnail($filename, $width, $height)->save($thumbFilename);
        }

        return Url::to('@web/' . $thumbFilename, true);
    }

    /**
     * @param \yii\base\Event $event
     *
     * @return mixed
     */
    protected function getValue($event)
    {
        return !$this->inProcess ? $this->flushImage() : $this->owner->{$this->imageAttribute};
    }

    /**
     * @param string    $name
     * @param bool|true $checkVars
     *
     * @return bool
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return $this->hasVirtualAttribute($name, 'Url')
               || $this->hasVirtualAttribute($name, 'Binary')
               || parent::canGetProperty($name, $checkVars);
    }

    /**
     * @param string $name
     *
     * @return bool|mixed|null|string
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if ($name == $this->imageAttribute && $this->absolutePath) {
            return $this->url($name);
        }

        if ($this->hasVirtualAttribute($name, 'Url')) {
            return $this->url($this->imageAttribute);
        }

        if ($this->hasVirtualAttribute($name, 'Binary')) {
            $local = \Yii::getAlias('@webroot/' . $this->owner->{$this->imageAttribute});
            if (file_exists($local)) {
                return file_get_contents($local);
            }

            return null;
        }

        return parent::__get($name);
    }

    /**
     * Returns a value indicating whether a method is defined.
     *
     * The default implementation is a call to php function `method_exists()`.
     * You may override this method when you implemented the php magic method `__call()`.
     *
     * @param string $name the method name
     *
     * @return boolean whether the method is defined
     */
    public function hasMethod($name)
    {
        return $name == self::THUMBNAIL_URL_METHOD || parent::hasMethod($name);
    }

    /**
     * Calls the named method which is not a class method.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when an unknown method is being invoked.
     *
     * @param string $name   the method name
     * @param array  $params method parameters
     *
     * @throws UnknownMethodException when calling unknown method
     * @return mixed the method return value
     */
    public function __call($name, $params)
    {
        if ($name == self::THUMBNAIL_URL_METHOD) {
            list($attribute, $size) = $params;

            return $this->thumbnail($attribute, $size);
        } else {
            return parent::__call($name, $params);
        }
    }

    /**
     * @return bool
     */
    private function flushImage()
    {
        $this->inProcess = true;

        /** @var ActiveRecord $record */
        $record = $this->owner;

        $binaryAttribute = $this->imageAttribute . 'Binary';

        /** @var ImageUpload $uploader */
        $uploader = null;

        if ($instance = UploadedFile::getInstance($record, $binaryAttribute)) {
            $uploader = ImageUpload::createFromEntity($record, $binaryAttribute, [
                'autoSave' => false
            ]);
        }

        if (!$record->isNewRecord && $uploader) {
            $path = \Yii::getAlias('@app/web/uploads/' . Inflector::camel2id((new \ReflectionClass($record))->getShortName())
                                   . DIRECTORY_SEPARATOR);
            if (file_exists($path)) {

                $basename = implode('-', $record->getPrimaryKey(true)) . '-' . $this->imageAttribute;

                $mask  = $basename. '*';
                $files = FileHelper::findFiles($path, ['only' => [$mask]]);

                foreach ($files as $file) {

                    if (file_exists($file)) {

                        unlink($file);
                    }
                }
            }
        }

        if ($uploader) {
            $this->resize($uploader);
            $this->createThumbnails($uploader);

            $uploader->toEntity($record, $this->imageAttribute);
            $record->save(false, [$this->imageAttribute]);
        }

        return $record->{$this->imageAttribute};
    }

    /**
     * @param $name
     *
     * @param $suffix
     *
     * @return bool
     */
    private function hasVirtualAttribute($name, $suffix)
    {
        return $this->imageAttribute . $suffix === $name;
    }

    /**
     * @param ImageUpload $uploader
     *
     * @return bool
     */
    private function createThumbnails($uploader)
    {
        if (!is_array($this->thumbnails)) {
            return false;
        }

        foreach ($this->thumbnails as $thumbnail) {
            list($width, $height) = $this->getDimension($thumbnail);
            $uploader->thumbnail($width, $height);
        }

        return true;
    }

    /**
     * @param ImageUpload $uploader
     *
     * @return bool
     */
    private function resize($uploader)
    {
        if (!$this->resize) {
            return false;
        }

        list($width, $height) = $this->getDimension($this->resize);
        $uploader->resize($width, $height);

        return true;
    }

    /**
     * @param $parameter
     *
     * @return array
     */
    private function getDimension($parameter)
    {
        if (is_array($parameter)) {
            $width  = ArrayHelper::getValue($parameter, 0, null);
            $height = ArrayHelper::getValue($parameter, 1, $width);

            return [$width, $height];
        }

        return [$parameter, $parameter];
    }
}