<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 13/01/15
 * Time: 15:21
 */

namespace yii2vm\db;

/**
 * Class ActiveQuery
 * @package yii2vm\db
 * @deprecated
 *
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @param string $function
     * @param array  $args
     *
     * @return mixed
     */
    public function __call($function, $args)
    {
        $methodName = 'scope' . $function;

        if (method_exists($this->modelClass, $methodName)) {
            return call_user_func_array([$this->modelClass, $methodName], array_merge([$this], $args));
        }

        return parent::__call($function, $args);
    }
}