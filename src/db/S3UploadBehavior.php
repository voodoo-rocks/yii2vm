<?php
namespace yii2vm\db;

use creocoder\flysystem\AwsS3Filesystem;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii2vm\media\upload\S3Upload;

/**
 * Class S3UploadBehavior
 * @package yii2vm\db
 */
class S3UploadBehavior extends AttributeBehavior
{
    /**
     * @var AwsS3Filesystem
     */
    public $adapter;

    /**
     * @var
     */
    public $imageAttribute;

    /**
     * @var bool
     */
    public $usePlaceholders = true;

    /**
     * @var bool
     */
    private $inProcess = false;

    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                ActiveRecord::EVENT_AFTER_INSERT => $this->imageAttribute,
                ActiveRecord::EVENT_AFTER_UPDATE => $this->imageAttribute,
            ];
        }
    }

    /**
     * @param      $attribute
     *
     * @return bool|string
     */
    public function url($attribute)
    {
        if ($this->owner->$attribute) {
            return Url::to(sprintf('%s/%s/%s',
                $this->adapter->baseUrl,
                $this->adapter->bucket,
                $this->owner->$attribute));
        }

        if ($this->usePlaceholders) {
            return 'http://placehold.it/500x500?text=No+Image';
        }

        return null;
    }

    /**
     * @param \yii\base\Event $event
     *
     * @return mixed
     */
    protected function getValue($event)
    {
        return !$this->inProcess ? $this->flushImage() : $this->owner->{$this->imageAttribute};
    }

    /**
     * @param string    $name
     * @param bool|true $checkVars
     *
     * @return bool
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return $this->hasVirtualAttribute($name, 'Url')
               || $this->hasVirtualAttribute($name, 'Binary')
               || parent::canGetProperty($name, $checkVars);
    }

    /**
     * @param string $name
     * @param bool   $checkVars
     *
     * @return bool
     */
    public function canSetProperty($name, $checkVars = true)
    {
        return $this->hasVirtualAttribute($name, 'Url')
               || $this->hasVirtualAttribute($name, 'Binary')
               || parent::canSetProperty($name, $checkVars);
    }

    /**
     * @param string $name
     *
     * @return bool|mixed|null|string
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if ($this->hasVirtualAttribute($name, 'Url')) {
            return $this->url($this->imageAttribute);
        }

        if ($this->hasVirtualAttribute($name, 'Binary')) {
            if ($this->adapter->has($this->owner->{$this->imageAttribute})) {
                return $this->adapter->read($this->owner->{$this->imageAttribute});
            }

            return null;
        }

        return parent::__get($name);
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @throws \yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {
        if ($this->hasVirtualAttribute($name, 'Url') || $this->hasVirtualAttribute($name, 'Binary')) {
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @return bool
     */
    private function flushImage()
    {
        $this->inProcess = true;

        /** @var ActiveRecord $record */
        $record = $this->owner;

        $binaryAttribute = $this->imageAttribute . 'Binary';

        /** @var S3Upload $uploader */
        $uploader = null;

        if ($instance = UploadedFile::getInstance($record, $binaryAttribute)) {
            $uploader = S3Upload::createFromEntity($record, $binaryAttribute, [
                'autoSave' => false,
                'adapter'  => $this->adapter,
            ]);
        }

        if ($uploader) {
            $uploader->toEntity($record, $this->imageAttribute);
            $record->save(false, [$this->imageAttribute]);
        }

        return $record->{$this->imageAttribute};
    }

    /**
     * @param $name
     * @param $suffix
     *
     * @return bool
     */
    private function hasVirtualAttribute($name, $suffix)
    {
        return $this->imageAttribute . $suffix === $name;
    }
}