<?php
namespace yii2vm\db;

use yii\base\Exception;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\helpers\Inflector;

class ActiveUtils
{
    /**
     * Returns an array representation of an array of records.
     *
     * @param array|Model     $records
     * @param array           $relations
     * @param callable | null $callable A method that could be used as a filter or additional data provider.
     *                                  If it returns an array it will be appended to the representation for each record.
     *                                  If it returns false or null it will skip this value
     *
     * @return array
     */
    public static function toArray($records, $relations = [], $callable = null)
    {
        if (!$records) {
            return [];
        }

        $result = [];

        if (!is_array($records)) {
            return self::recordToArray($records, $relations, $callable);
        }

        /** @var ActiveRecord $record */
        foreach ($records as $record) {
            $result[] = self::recordToArray($record, $relations, $callable);
        }

        return $result;
    }

    /**
     * Returns an array representation of a record.
     * Relations could be passed only if the record is an object of ActiveRecord or its subclass
     *
     * @param Model $record
     * @param array $relations
     *
     * @return array|void
     * @throws Exception The exception will be thrown if the passed record is not an object of ActiveRecord
     * and relations are not an empty array or null
     */
    private static function recordToArray($record, $relations = [], $callable = null)
    {
        /** @var BaseActiveRecord $model */
        $attributes = $record->toArray([], [], true);

        if ($record instanceof BaseActiveRecord) {
            foreach ($relations as $relation) {
                $attributes += [
                    Inflector::underscore($relation) => self::toArray($record->{$relation})
                ];
            }

            // If callable is not null it can be a filter or extra data provider
            if ($callable) {
                $extra = call_user_func($callable, $record);

                // If it returns null or false just skip it
                // If it returns an array just add it to an entity
                if ($extra && is_array($extra)) {
                    $attributes += $extra;
                }
            }
        } else if (count($relations)) {
            throw new Exception('Relations are supported for ActiveRecords only');
        }

        return $attributes;
    }
}