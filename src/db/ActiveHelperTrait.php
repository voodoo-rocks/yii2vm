<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/05/15
 * Time: 15:39
 */

namespace yii2vm\db;

use Yii;
use yii\db\ActiveQueryTrait;

/**
 * Class ActiveHelperTrait
 * @package yii2vm\db
 *
 * @deprecated
 */
trait ActiveHelperTrait
{
    use ActiveQueryTrait;

    /**
     * @return mixed
     */
    public static function random()
    {
        return static::getRandom()->one();
    }

    /**
     * @param ActiveQuery $query
     *
     * @return $this
     */
    public static function scopeRandom(ActiveQuery $query)
    {
        return $query->addOrderBy('rand()');
    }

    /**
     * @return mixed
     */
    public static function getRandom()
    {
        return static::find()->addOrderBy('rand()');
    }
}