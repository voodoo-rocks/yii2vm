<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 13/01/15
 * Time: 15:19
 */

namespace yii2vm\db;

/**
 * Class ActiveScopeTrait
 * @package yii2vm\db
 * @deprecated
 */
trait ActiveScopeTrait
{
    /**
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return \Yii::createObject(\yii2vm\db\ActiveQuery::className(), [get_called_class()]);
    }
}