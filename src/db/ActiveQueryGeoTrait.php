<?php
namespace yii2vm\db;
use yii2vm\geo\Coordinates;

/**
 * Class ActiveQuery
 * @package yii2vm\db
 */
trait ActiveQueryGeoTrait
{

    public function nearby($lat, $lng, $radius)
    {
        /** @var ActiveQuery $this */
        return $this->andWhere('DEGREES(ACOS(COS(RADIANS(lat)) * COS(RADIANS(:lat2)) *
             COS(RADIANS(lng) - RADIANS(:lng2)) +
             SIN(RADIANS(lat)) * SIN(RADIANS(:lat2)))) * :miles < :radius',
            [
                'lat2'   => $lat,
                'lng2'   => $lng,
                'radius' => $radius,
                'miles'  => Coordinates::MILES_AT_DEGREE
            ]
        );
    }

    public function inArea($leftTopLat, $leftTopLng, $rightBottomLat, $rightBottomLng)
    {

        $maxY = Coordinates::latToY($leftTopLat);
        $minX = Coordinates::lonToX($leftTopLng);

        $minY = Coordinates::latToY($rightBottomLat);
        $maxX = Coordinates::lonToX($rightBottomLng);

        /** @var ActiveQuery $this */
        return $this->addSelect([
            '*',
            'round(:EARTH_IN_PIXELS + :EARTH_RADIUS_PIXELS * lng * PI() / 180) as X',
            'round(:EARTH_IN_PIXELS - :EARTH_RADIUS_PIXELS * log((1 + sin(lat * pi() / 180)) / (1 - sin(lat * pi() / 180)))
             / 2) as Y'
        ])->having('(:minX < X) AND (X < :maxX) AND (:minY < Y) AND (Y < :maxY)',
            [
                'minX' => $minX,
                'maxX' => $maxX,
                'minY' => $minY,
                'maxY' => $maxY,
                'EARTH_IN_PIXELS' => Coordinates::HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS,
                'EARTH_RADIUS_PIXELS' => Coordinates::EARTH_RADIUS_PIXELS
            ]
        );
    }
}