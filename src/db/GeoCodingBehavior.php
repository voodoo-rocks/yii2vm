<?php
namespace yii2vm\db;

use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;
use yii2vm\components\GoogleGeoCoder;

/**
 * Class GeoCodingBehavior
 *
 * !!! Require yii2vm\components\GoogleGeoCoder component with name geoCoder (ex. Yii::$app->geoCoder) !!!
 *
 * Config example
 * [
 *      'class' => GeoCodingBehavior::className(),
 *      'attributeName' => 'country',     // Target attribute
 *      'sources' => ['city', 'address'], //Fields in model
 *      'format' => [
 *          ['type' => GoogleGeoCoder::TYPE_COUNTRY, 'style' => GoogleGeoCoder::STYLE_LONG]
 *       ]
 * ]
 *
 * @package yii2vm\db
 */
class GeoCodingBehavior extends AttributeBehavior
{
    public $attributeName;

    public $sources;

    public $format = ['type' => GoogleGeoCoder::TYPE_FORMATTED_ADDRESS];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->attributeName,
                BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->attributeName,
            ];
        }
    }

    protected function getValue($event)
    {
        $queryUrl = $this->getQueryUrl();
        return $this->query($queryUrl);
    }

    protected function getQueryUrl()
    {
        if (is_array($this->sources)) {
            $sourcesData = [];
            foreach ($this->sources as $sourceAttribute) {
                $sourcesData[] = $this->owner->{$sourceAttribute};
            }

            return implode(', ', $sourcesData);
        }

        return $this->owner->{$this->sources};
    }

    protected function query($query)
    {
        /** @var GoogleGeoCoder $geoCoder */
        $geoCoder = \Yii::$app->geoCoder;

        return $geoCoder->query($query)->fetch($this->format);
    }
}