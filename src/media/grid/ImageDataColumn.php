<?php
namespace yii2vm\media\grid;

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;

class ImageDataColumn extends DataColumn
{
    public $thumbnailWidth  = 150;
    public $thumbnailHeight = 150;

    protected function renderDataCellContent($model, $key, $index)
    {
        $uri = sprintf('http://placehold.it/%dx%d', $this->thumbnailWidth, $this->thumbnailHeight);

        if ($model->{$this->attribute}) {
            $uri = Url::base(true) . '/' . $model->{$this->attribute};
        }

        return Html::img($uri, ['class' => 'image-data-column']);
    }
}