<?php
namespace yii2vm\media\upload;

use creocoder\flysystem\AwsS3Filesystem;
use Yii;
use yii\db\ActiveRecordInterface;
use yii\helpers\Inflector;

/**
 * Class S3Upload
 * @package yii2vm\media\upload
 */
class S3Upload extends BinaryUpload
{
    /**
     * @var AwsS3Filesystem
     */
    public $adapter;

    /**
     * @param null $filename
     *
     * @return bool|null|string
     */
    public function toFile($filename = null)
    {
        if (!$this->handler->isValid() || !$this->adapter) {
            return false;
        }

        if (!$filename) {
            $filename = md5(uniqid());
        }

        $this->adapter->put($filename, $this->handler->getContent());

        $mime = $this->adapter->getMimetype($filename);
        if ($mime && ($mime = explode('/', $mime))) {

            $extension = end($mime);
            $this->adapter->rename($filename, $filename . '.' . $extension);
            $filename .= '.' . $extension;
        }

        return $filename;
    }

    /**
     * @param ActiveRecordInterface $entity
     * @param                       $attribute
     * @param null                  $extension
     *
     * @return BinaryUpload
     */
    public function toEntity($entity, $attribute, $extension = null)
    {
        $basename = implode('-', $entity->getPrimaryKey(true)) . '-' . $attribute;
        if ($extension) {
            $basename = $basename . '.' . $extension;
        }

        $filename =
            Inflector::camel2id((new \ReflectionClass($entity))->getShortName())
            . DIRECTORY_SEPARATOR . $basename;

        $result = $this->toFile($filename);

        if ($result !== false) {
            $entity->setAttribute($attribute, $result);

            if ($this->autoSave && !$entity->save()) {
                return false;
            }
        }

        return $result;
    }
} 