<?php
namespace yii2vm\media\upload;

use Exception;
use yii\base\Component;
use yii\base\ErrorException;
use yii\db\ActiveRecordInterface;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii2vm\media\upload\handlers\Base64UploadHandler;
use yii2vm\media\upload\handlers\BaseUploadHandler;
use yii2vm\media\upload\handlers\EntityUploadHandler;
use yii2vm\media\upload\handlers\UriUploadHandler;

/**
 * Class BinaryUpload
 * @package yii2vm\media\upload
 */
class BinaryUpload extends Component
{
    /**
     * @var string
     */
    public $target = 'uploads';

    public $autoSave = true;
    /**
     * @var BaseUploadHandler
     */
    protected $handler;

    /**
     * @param array $handler
     * @param array $config
     */
    public function __construct($handler, $config = [])
    {
        parent::__construct($config);

        $this->handler = $handler;
    }

    /**
     * @param       $base64
     *
     * @param array $config
     *
     * @return static
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromBase64($base64, $config = [])
    {
        return \Yii::createObject(self::className(), [
            new Base64UploadHandler($base64),
            $config
        ]);
    }

    /**
     * @param       $uri
     *
     * @param array $config
     *
     * @return static
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromUri($uri, $config = [])
    {
        return \Yii::createObject(self::className(), [
            new UriUploadHandler($uri),
            $config
        ]);
    }

    /**
     * @param       $entity
     * @param       $attribute
     *
     * @param array $config
     *
     * @return static
     * @throws \yii\base\InvalidConfigException
     */
    public static function createFromEntity($entity, $attribute, $config = [])
    {
        return \Yii::createObject(self::className(), [
            new EntityUploadHandler($entity, $attribute),
            $config
        ]);
    }

    /**
     * @param ActiveRecordInterface $entity
     * @param                       $attribute
     * @param null                  $extension
     *
     * @return BinaryUpload
     */
    public function toEntity($entity, $attribute, $extension = null)
    {
        $filename = self::generateFilename($entity, $attribute, $extension);

        $result = $this->toFile($filename);

        if ($result !== false) {
            $entity->setAttribute($attribute, $result);

            if ($this->autoSave && !$entity->save()) {
                return false;
            }
        }

        return $result;
    }

    /**
     * @param string $filename
     *
     * @return $this
     * @throws Exception
     */
    public function toFile($filename = null)
    {
        if (!$this->handler->isValid()) {
            return false;
        }

        if (!$filename) {
            $filename = md5(uniqid());
        }

        $attributeFilename = $this->target . DIRECTORY_SEPARATOR . $filename;
        $filename          = \Yii::getAlias('@app/web/' . $attributeFilename);

        $path      = pathinfo($filename, PATHINFO_DIRNAME);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        try {
            FileHelper::createDirectory($path);
        } catch (ErrorException $e) {
            throw new Exception(sprintf('Cannot create the folder %s because %s', $path, $e->getMessage()));
        }

        file_put_contents($filename, $this->handler->getContent());

        if (!$extension) {
            $mime = mime_content_type($filename);
            if ($mime && ($mime = explode('/', $mime))) {
                $extension = end($mime);
                rename($filename, $filename . '.' . $extension);
                $attributeFilename = $attributeFilename . '.' . $extension;
            }
        }

        return $attributeFilename;
    }

    /**
     * @param ActiveRecordInterface $entity
     * @param                       $attribute
     * @param                       $extension
     *
     * @return string
     */
    public static function generateFilename($entity, $attribute, $extension = null)
    {
        $basename = implode('-', $entity->getPrimaryKey(true)) . '-' . $attribute;
        if ($extension) {
            $basename = $basename . '.' . $extension;
        }

        $filename =
            Inflector::camel2id((new \ReflectionClass($entity))->getShortName())
            . DIRECTORY_SEPARATOR . $basename;

        return $filename;
    }
} 