<?php
namespace yii2vm\media\upload\handlers;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class EntityUploadHandler extends BaseUploadHandler
{
    /**
     * @var ActiveRecord
     */
    private $entity;
    private $attribute;

    function __construct(ActiveRecord $entity, $attribute)
    {
        $this->entity    = $entity;
        $this->attribute = $attribute;
    }

    public function getContent()
    {
        $uploaded = UploadedFile::getInstance($this->entity, $this->attribute);

        return $uploaded ? file_get_contents($uploaded->tempName) : null;
    }

    public function isValid()
    {
        return UploadedFile::getInstance($this->entity, $this->attribute) != null;
    }
}