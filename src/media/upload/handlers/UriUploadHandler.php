<?php
namespace yii2vm\media\upload\handlers;

class UriUploadHandler extends BaseUploadHandler
{
    private $uri;

    function __construct($uri)
    {
        $this->uri = $uri;
    }

    public function getContent()
    {
        return file_get_contents($this->uri);
    }

    public function isValid()
    {
        return $this->getContent() != null;
    }
}