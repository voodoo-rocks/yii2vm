<?php
namespace yii2vm\media\upload\handlers;

class Base64UploadHandler extends BaseUploadHandler
{
    private $content;

    function __construct($base64)
    {
        $this->content = base64_decode($base64);
    }

    public function getContent()
    {
        return $this->content;
    }

    public function isValid()
    {
        return $this->content != null;
    }
} 