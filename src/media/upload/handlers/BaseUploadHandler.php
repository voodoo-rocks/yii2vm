<?php
namespace yii2vm\media\upload\handlers;

class BaseUploadHandler
{
    public function getContent()
    {
        return null;
    }

    public function isValid()
    {
        return false;
    }
}