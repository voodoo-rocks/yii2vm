<?php
namespace yii2vm\media\upload;

use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;

class ImageUpload extends BinaryUpload
{
    const RESIZE_MODE_OUTBOUND = ManipulatorInterface::THUMBNAIL_OUTBOUND;
    const RESIZE_MODE_INSET    = ManipulatorInterface::THUMBNAIL_INSET;

    private $thumbnails = [];

    public function thumbnail($width, $height = null, $mode = self::RESIZE_MODE_OUTBOUND)
    {
        return $this->addThumbnail($width, $height, false, $mode);
    }

    private function addThumbnail($width, $height, $replace, $mode)
    {
        $this->thumbnails[] = (object)[
            'width'       => $width,
            'height'      => $height ?: $width,
            'replacement' => $replace,
            'mode'        => $mode
        ];

        return $this;
    }

    /**
     * @param        $width
     * @param null   $height
     * @param string $mode
     *
     * @return $this|ImageUpload
     */
    public function resize($width, $height = null, $mode = self::RESIZE_MODE_OUTBOUND)
    {
        return $this->addThumbnail($width, $height, true, $mode);
    }

    public function toFile($filename = null)
    {
        $filename = parent::toFile($filename);
        if ($filename === false) {
            return false;
        }

        foreach ($this->thumbnails as $thumbnail) {

            $thumbnailFilename = self::thumbnailFilename(
                $filename, $thumbnail->width, $thumbnail->height);

            Image::thumbnail($filename,
                $thumbnail->width,
                $thumbnail->height,
                $thumbnail->mode)->save($thumbnailFilename);
        }

        return $filename;
    }

    public static function thumbnailFilename($filename, $width, $height)
    {
        $info = (object)pathinfo($filename);

        return $info->dirname . DIRECTORY_SEPARATOR .
               sprintf('%s-%dx%d.%s', $info->filename, $width, $height, $info->extension);
    }
} 