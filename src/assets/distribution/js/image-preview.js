;
'use strict';
(function ($) {
    var wrapperClass = 'image-preview__wrapper';
    var fileClass = 'image-preview__file';
    var filenameClass = 'image-preview__filename';
    var imageClass = 'image-preview__image';
    var removeClass = 'image-preview__remove';
    var helpClass = 'image-preview__help';
    var placeholderClass = 'image-preview__placeholder';

    var wrapperTemplate = '<span class="' + wrapperClass + '"></span>';
    var imageTemplate = '<span class="' + imageClass + '"></span>';
    var removeTemplate = '<button type="button" class="' + removeClass + '" title="Remove image">&times;</button>';
    var errorTemplate = '<div class="' + helpClass + ' text-danger"></div>';

    var ImagePreview = function (file, settings) {
        var self = this;

        self.settings = {
            placeholder: '',
            extensions: ['gif', 'png', 'jpg', 'jpeg'],
            maxSize: 10,
            alt: '',
            submitOnChange: false
        };

        self.file = file;

        $.extend(self.settings, file.data(), settings || {});

        if(!self.settings.placeholder){
            self.settings.placeholder = self.settings.asset + '/images/placeholder.png';
        }

        self.init.call(self);

        self.file.on('change', function () {
            self.onUpdate.call(self);
        });

        self.removeButton.on('click', function (event) {
            event.preventDefault();

            self.onReset.call(self);
            self.settings.update ? self.settings.update.call(self) : self;

            if (self.settings.submitOnChange) {
                self.onSubmit.call(self);
            }
        });

        return self;
    };

    ImagePreview.prototype.onReset = function () {
        this.wrapper.wrap('<form/>');
        this.wrapper.parent().trigger('reset');
        this.wrapper.find(':input').val('');
        this.wrapper.unwrap();

        this.buildPlaceholder.call(this);

        return this;
    };

    ImagePreview.prototype.onError = function (message) {
        console.error(message);

        this.errors.push(message);
        this.help.html(message);

        this.onReset.call(this);

        return this.settings.error ? this.settings.error.call(this) : this;
    };

    ImagePreview.prototype.createObjectURL = function (file) {
        if (window.URL && window.URL.createObjectURL) {
            return window.URL.createObjectURL(file);
        } else if (window.webkitURL) {
            return window.webkitURL.createObjectURL(file);
        }
        return this.onError.call(this, 'Error with create blob url.');
    };

    ImagePreview.prototype.onSubmit = function () {
        var self = this;
        var form = self.file.closest('form').get(0);
        var url, method, data, formWrapper;

        method = form && form.method ? form.method : 'post';
        url = form && form.action ? form.action : self.settings.url || window.location.href;

        formWrapper = self.wrapper.wrap('<form/>', {enctype: 'multipart/form-data'}).parent().get(0);
        data = new FormData(formWrapper);
        self.wrapper.unwrap();

        $.ajax({
            url: url,
            method: method,
            data: data,
            processData: false,
            contentType: false
        })

            .done(function (response) {
                if (self.settings.ajaxDone) {
                    self.settings.ajaxDone.call(self, response);
                }
            })

            .fail(function (response) {
                self.onError.call(self, 'HTTP request returned fail');
                if (self.settings.ajaxFail) {
                    self.settings.ajaxFail.call(self, response);
                }
            })

            .always(function (response) {
                if (self.settings.ajaxAlways) {
                    self.settings.ajaxAlways.call(self, response);
                }
            });

        return this;
    };

    ImagePreview.prototype.onUpdate = function () {
        var blob, file;
        var ext = this.file.val().split('.').pop().toLowerCase();

        if ($.inArray(ext, this.settings.extensions) == -1) {
            return this.onError.call(this, 'You can only select the picture with extension ' + this.settings.extensions + '.');
        }

        file = this.file.prop('files')[0];

        if (file.size > this.settings.maxSize * 1024 * 1024) {
            return this.onError.call(this, 'The file must be less than ' + this.settings.maxSize + 'MB.');
        }

        blob = this.createObjectURL(file);

        if (!blob) {
            return this.onError.call(this, 'Your browser is very old!');
        }

        this.image.css({backgroundImage: 'url(' + blob + ')'});

        this.help.html('');

        if (this.settings.submitOnChange) {
            this.onSubmit.call(this);
        }

        return this.settings.update ? this.settings.update.call(this) : this;
    };

    ImagePreview.prototype.buildPlaceholder = function () {
        this.image.css({backgroundImage: 'url(' + this.settings.placeholder + ')'});
        this.image.addClass(placeholderClass);
        this.isPlaceholder = true;

        return this;
    };

    ImagePreview.prototype.buildImage = function (src) {
        var self = this;
        var img = $('<img/>', {src: src});

        img.load(function () {
            self.image.css({backgroundImage: 'url(' + src + ')'});
            self.image.removeClass(placeholderClass);
            self.isPlaceholder = false;
        }).error(function (error) {
            console.error(error);
            self.buildPlaceholder.call(self);
        });

        return self;
    };

    ImagePreview.prototype.buildWrapper = function () {
        this.file.siblings('.help-block').append(errorTemplate);
        this.file.wrap(wrapperTemplate);
        this.file.after(imageTemplate, removeTemplate);
    };

    ImagePreview.prototype.init = function () {
        if(!this.file.parent().hasClass(wrapperClass)){
            this.buildWrapper.call(this);
        }

        this.file.addClass(fileClass);
        this.wrapper = this.file.closest('.' + wrapperClass);
        this.filename = $(this.file.data('filename')).addClass(filenameClass).appendTo(this.wrapper);
        this.image = this.wrapper.find('.' + imageClass);
        this.removeButton = this.wrapper.find('.' + removeClass);
        this.help = this.wrapper.siblings('.help-block').find('.' + helpClass);
        this.errors = [];
        this.isPlaceholder = !this.filename.val();

        this.isPlaceholder ? this.buildPlaceholder.call(this) : this.buildImage.call(this, this.settings.src);

        return this;
    };

    $.fn.imagePreview = function () {
        var settings = {};

        $.each(arguments, function () {
            if (typeof this === 'object') {
                settings = this;
            }
        });

        this.each(function(){
            new ImagePreview($(this), settings);
        });
    };
}(jQuery));