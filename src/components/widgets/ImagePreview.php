<?php

namespace yii2vm\components\widgets;

use Yii;
use yii\widgets\InputWidget;

class ImagePreview extends InputWidget
{
	public $options;

	public function run()
	{
		return $this->render('image-preview', [
			'model'      => $this->model,
			'attribute'  => $this->attribute,
		    'options'    => $this->options
		]);
	}
}