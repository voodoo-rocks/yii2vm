<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii2vm\components\Yii2VmAsset;

/* @var $model yii\base\Model */
/* @var $attribute String */
/* @var $options Array */

$asset = Yii2VmAsset::register($this)->baseUrl;
$attributeFilename = $attribute . '_filename';
$unique = md5(uniqid());

$defaultOptions = [
	'class'    => 'image-preview',
	'data-src' => $model->{$attributeFilename} ? Url::to('@web/' . $model->{$attributeFilename}) : false,
	'data-url' => Url::to(''),
	'data-filename' => '#field-filename-' . $unique,
	'data-asset' => $asset
];

$commonOptions = ArrayHelper::merge($defaultOptions, $options);

echo Html::activeFileInput($model, $attribute, $commonOptions);

echo Html::activeHiddenInput($model, $attributeFilename, ['id' => 'field-filename-' . $unique]);

?>