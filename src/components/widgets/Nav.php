<?php
namespace yii2vm\components\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class Nav
 * @package yii2vm\components\widgets
 */
class Nav extends \yii\bootstrap\Nav
{
    /**
     * @param array|string $item
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_array($item)) {
            $icon = ArrayHelper::getValue($item, 'icon');
            if ($icon) {
                $label         = ArrayHelper::getValue($item, 'label');
                $item['label'] = Html::tag('i', null, ['class' => $icon]) . PHP_EOL . $label;
            }
        }

        return parent::renderItem($item);
    }
}