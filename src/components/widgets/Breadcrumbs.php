<?php
namespace yii2vm\components\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class Breadcrumbs
 * @package yii2vm\src\components
 */
class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    /**
     * @var string the name of the breadcrumb container tag.
     */
    public $tag = 'ol';

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration.
     */
    public function init()
    {
        if (Yii::$app->controller->action->id != Yii::$app->controller->defaultAction) {
            $custom = [
                [
                    'label' => Inflector::camel2words(Yii::$app->controller->id),
                    'url'   => [Yii::$app->controller->id . '/' . Yii::$app->controller->defaultAction]
                ],
                [
                    'label' => Inflector::camel2words(Yii::$app->controller->action->id)
                ]
            ];
        } else {
            $custom = [
                [
                    'label' => Inflector::camel2words(Yii::$app->controller->id)
                ]
            ];
        }

        $this->links = ArrayHelper::getValue(Yii::$app->controller->view->params, 'breadcrumbs', $custom);

        parent::init();
    }
}