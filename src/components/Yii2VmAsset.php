<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 20/01/15
 * Time: 15:51
 */

namespace yii2vm\components;

use yii\web\AssetBundle;

class Yii2VmAsset extends AssetBundle
{
    public $sourcePath = '@vendor/voodoo-rocks/yii2vm/src/assets/';

	public $css = [
		'css/polyfill.object-fit.min.css',
		'css/yii2vm.min.css'
	];

    public $js = [
	    'js/polyfill.object-fit.min.js',
        'js/yii2vm.min.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}