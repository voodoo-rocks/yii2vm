<?php
namespace yii2vm\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class GoogleGeoCoder extends Component
{
    public $apiKey;

    protected $response = [];

    const STYLE_LONG = 'long_name';

    const STYLE_SHORT = 'short_name';

    /** 94043 */
    const TYPE_POSTAL_CODE = 'postal_code';

    /**
     * "long_name" : "United States"
     * "short_name" : "US"
     */
    const TYPE_COUNTRY = 'country';

    /**
     * "long_name" : "California",
     * "short_name" : "CA"
     */
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_1 = 'administrative_area_level_1';

    /**
     * "long_name" : "Santa Clara County"
     * "short_name" : "Santa Clara County"
     */
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_2 = 'administrative_area_level_2';

    /**
     * "long_name" : "Mountain View",
     * "short_name" : "Mountain View",
     */
    const TYPE_LOCALITY = 'locality';

    /**
     * "long_name" : "Amphitheatre Parkway",
     * "short_name" : "Amphitheatre Pkwy"
     */
    const TYPE_ROUTE = 'route';

    /**
     * "formatted_address" : "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",
     */
    const TYPE_FORMATTED_ADDRESS = 'formatted_address';

    const TYPE_LAT = 'lat';

    const TYPE_LNG = 'lng';

    /**
     * @param string $query
     *
     * @return $this
     */
    public function query($query)
    {
        $this->response = [];
        $queryUrl = $this->buildUrl(str_replace(' ','+',$query));
        $this->response = $this->getContent($queryUrl);
        return $this;
    }

    /**
     *
     */
    public function flush()
    {
        $this->response = [];
    }

    /**
     * @param array $types //contain arrays with formats
     *
     * @return string
     */
    public function fetch(array $types = [])
    {
        $data = '';
        foreach ($types as $typeData) {
            if ($data !== '') {
                $data .= ', ';
            }
            $data .= $this->getFromResponse(
                ArrayHelper::getValue($typeData, 'type'),
                ArrayHelper::getValue($typeData, 'style', self::STYLE_LONG)
            );
        }

        return $data;
    }

    /**
     * @param string $type
     * @param string $style
     *
     * @return mixed|null|string
     */
    protected function getFromResponse($type, $style)
    {
        if (!$this->response) {
            return '';
        }

        if ($type === self::TYPE_FORMATTED_ADDRESS) {
            return ArrayHelper::getValue($this->response, ['results', '0', self::TYPE_FORMATTED_ADDRESS], '');
        }

        if ($type === self::TYPE_LAT) {
            return ArrayHelper::getValue($this->response, ['results', '0', 'geometry', 'location', 'lat'], '');
        }

        if ($type === self::TYPE_LNG) {
            return ArrayHelper::getValue($this->response, ['results', '0', 'geometry', 'location', 'lng'], '');
        }

        return $this->findComponentInResponse($type, $this->response, $style);
    }

    /**
     * @param string $type
     * @param array $response
     * @param string $style
     *
     * @return mixed|null
     */
    protected function findComponentInResponse($type, $response, $style)
    {
        $addressComponents = ArrayHelper::getValue($response, ['results', '0', 'address_components']);
        if (!$addressComponents) {
            return null;
        }

        foreach ($addressComponents as $addressComponent) {
            if (in_array($type, $addressComponent['types'])) {

                return ArrayHelper::getValue($addressComponent, $style, '');
            }
        }

        return null;
    }

    /**
     * @param $query
     *
     * @return string
     */
    protected function buildUrl($query)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . trim($query);

        if ($this->apiKey) {
            $url .= '&key=' . $this->apiKey;
        }

        return $url;
    }

    /**
     * @param string $url
     *
     * @return mixed|null
     */
    protected function getContent($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        if (empty($response)) {
            return null;
        }

        $response = Json::decode($response);

        if (ArrayHelper::getValue($response, 'status') !== 'OK') {
            return null;
        }

        return $response;
    }
}