<?php

namespace yii2vm\components;

use stdClass;
use Yii;
use yii\db;

/**
 * Class ObjectUtils
 * @package yii2vm\utils
 *
 * @deprecated
 */
class ObjectUtils
{
    /**
     * @param array $array
     *
     * @return mixed
     */
    public static function fromArray($array)
    {
        $object = new stdClass;
        foreach ($array as $key => $value) {
            if (strlen($key)) {
                if (is_array($value)) {
                    $object->{$key} = self::fromArray($value); //RECURSION
                } else {
                    $object->{$key} = $value;
                }
            }
        }

        return $object;
    }

    /**
     * @param $object
     *
     * @return array
     */
    public static function toArray($object)
    {
        if (is_object($object)) {
            $object = (array)$object;
        }
        if (is_array($object)) {
            $result = [];
            foreach ($object as $key => $value) {
                $result[$key] = self::toArray($value);
            }
        } else {
            $result = $object;
        }

        return $result;
    }
}