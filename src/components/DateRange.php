<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/05/15
 * Time: 00:36
 */

namespace yii2vm\components;

use DateInterval;
use DateTime;
use yii\base\Component;
use yii\base\Exception;

/**
 * Class DateRange
 * @package yii2vm\components
 *
 * @deprecated
 */
class DateRange extends Component
{
    /**
     * @var array
     */
    private $dates = [];

    /**
     * @param       DateTime $start
     * @param null| DateTime $end
     *
     * @return DateRange
     * @throws Exception
     */
    public static function createFromDates($start, $end)
    {
        if ($start > $end) {
            list($start, $end) = [$end, $start];
        }

        if (!is_a($start, DateTime::class) || (!is_a($end, DateTime::class))) {
            throw new Exception('Start and end dates must be objects of DateTime');
        }

        $current = clone $start;

        $range = new DateRange([]);

        do {
            $range->dates[] = $current;
            $toArray        = clone $current;
            $current        = $toArray->add(\DateInterval::createFromDateString('1 day'));
        } while ($current <= $end);

        return $range;
    }

    /**
     * @param DateTime|null       $start
     * @param DateInterval|string $interval
     *
     * @return DateRange
     * @throws Exception
     */
    public static function createFromInterval($start = null, $interval = '1 day')
    {
        if (!$start) {
            $start = new DateTime('today');
        }

        if (is_string($interval)) {
            $interval = \DateInterval::createFromDateString($interval);
        }

        $end = clone $start;

        return self::createFromDates($start, $end->add($interval));
    }

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return DateTime
     */
    public function getStart()
    {
        return reset($this->dates);
    }

    public function start($format = 'Y-m-d')
    {
        return $this->getStart()->format($format);
    }

    /**
     * @return DateTime
     */
    public function getEnd()
    {
        return end($this->dates);
    }

    public function end($format = 'Y-m-d')
    {
        return $this->getEnd()->format($format);
    }

    /**
     * @param array $format
     *
     * @return array
     */
    public function toArray($format = null)
    {
        $result = null;

        if ($format) {
            $result = [];

            /** @var DateTime $date */
            foreach ($this->dates as $date) {
                $result[] = $date->format($format);
            }
        }

        return $result ?: $this->dates;
    }

    public function inverse()
    {
        $this->dates = array_reverse($this->dates);
    }
}