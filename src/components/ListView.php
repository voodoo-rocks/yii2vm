<?php
namespace yii2vm\components;

use yii\helpers\Html;

/**
 * Class ListView
 * @package yii2vm\components
 */
class ListView extends \yii\widgets\ListView
{
    public $simpleList = false;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        if ($this->simpleList) {
            $this->layout = Html::tag('div', '{items}', ['class' => 'row']);
        } else {
            $this->layout = '{summary}' . Html::tag('div', '{items}', ['class' => 'row']) . '{pager}';
        }

        parent::init();
    }

}