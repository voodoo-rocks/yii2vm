<?php
namespace yii2vm\images;

class PlaceHoldItPlaceholder extends Placeholder
{
    public $text = 'No+image';

    public function init()
    {
        parent::init();

        if (!$this->value) {
            $this->value = function ($width, $height) {
                return sprintf('http://placehold.it/%sx%s?text=%s', $width, $height, $this->text);
            };
        }
    }
}