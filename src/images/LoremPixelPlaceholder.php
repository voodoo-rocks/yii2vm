<?php
namespace yii2vm\images;

/**
 * Class LoremPixelPlaceholder
 * @package yii2vm\images
 *
 *          Default implementation of Placeholder using Lorem Pixel service for generating placeholders
 */
class LoremPixelPlaceholder extends Placeholder
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        if (!$this->value) {
            $this->value = function ($width, $height) {
                return sprintf('http://lorempixel.com/%d/%d', $width, $height);
            };
        }
    }

}