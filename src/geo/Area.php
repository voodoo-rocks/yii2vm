<?php
namespace yii2vm\geo;

class Area
{
    public $leftTopLat;
    public $leftTopLng;
    public $rightBottomLat;
    public $rightBottomLng;

    public function __construct($leftTopLat, $leftTopLng, $rightBottomLat, $rightBottomLng)
    {
        $this->leftTopLat = $leftTopLat;
        $this->leftTopLng = $leftTopLng;
        $this->rightBottomLat = $rightBottomLat;
        $this->rightBottomLng = $rightBottomLng;
    }

    public function asArray()
    {
        return [
            'leftTop' => [
                'lat' => $this->leftTopLat,
                'lng' => $this->leftTopLng
            ],
            'rightBottom' => [
                'lat' => $this->rightBottomLng,
                'lng' => $this->rightBottomLat
            ]
        ];
    }

    public function getLeftTop($asObject = false)
    {
        $leftTop = [
            'lat' => $this->leftTopLat,
            'lng' => $this->leftTopLng
        ];

        if ($asObject) {
            return (object)$leftTop;
        }

        return $leftTop;
    }

    public function getRightBottom($asObject = false)
    {
        $rightBottom = [
            'lat' => $this->rightBottomLat,
            'lng' => $this->rightBottomLng
        ];

        if ($asObject) {
            return (object)$rightBottom;
        }

        return $rightBottom;
    }
}