<?php

/*
 * Converts geo degree -> km and km -> geo degree approximately
 * Transfer geo coord to Cartesian coordinate, use pixels at google map zoom level 21 like a points
*/

namespace yii2vm\geo;

class Coordinates
{
    const METERS_PER_DEGREE_LATITUDE            = 11132.22;
    const EARTH_PERIMETER_METERS                = 40076000;
    const EARTH_RADIUS_METERS                   = 6378293;
    const TOTAL_DEGREES                         = 360;
    const EARTH_RADIUS_PIXELS                   = 85445659.447; // HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXEL / pi()
    const HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS = 268435456; //at zoom level 21
    const MILES_AT_DEGREE = 63;

    public static function metersToDegrees($meters)
    {
        return $meters / self::EARTH_PERIMETER_METERS * self::TOTAL_DEGREES;
    }

    public static function degreesToMeters($degrees)
    {
        return $degrees / self::TOTAL_DEGREES * self::EARTH_PERIMETER_METERS;
    }

    public static function lonToX($lon)
    {
        return round(self::HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS + self::EARTH_RADIUS_PIXELS * $lon * pi() / 180);
    }

    public static function latToY($lat)
    {
        return round(self::HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS - self::EARTH_RADIUS_PIXELS *
                                                                   log((1 + sin($lat * pi() / 180)) /
                                                                       (1 - sin($lat * pi() / 180))) / 2);
    }

    public static function latToMercatorY($lat,
                                          $width = self::HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS,
                                          $height = self::HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS)
    {
        return ($height/2)-($width*log(tan((M_PI/4)+(($lat*M_PI/180)/2)))/(2*M_PI));
    }

    public static function lngToMercatorX($lng,
                                          $width = self::HALF_OF_EARTH_CIRCUMFERENCE_IN_PIXELS)
    {
        return ($lng+180)*($width/360);
    }

    public static function pixelDistance($lat1, $lon1, $lat2, $lon2)
    {
        $x1 = self::lonToX($lon1);
        $y1 = self::latToY($lat1);

        $x2 = self::lonToX($lon2);
        $y2 = self::latToY($lat2);

        return sqrt(pow(($x1 - $x2), 2) + pow(($y1 - $y2), 2));
    }

    public static function distance($lat1, $lng1, $lat2, $lng2)
    {
        $latd = deg2rad($lat2 - $lat1);
        $lond = deg2rad($lng2 - $lng1);
        $a = sin($latd / 2) * sin($latd / 2) +
             cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
             sin($lond / 2) * sin($lond / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return 6371.0 * $c;
    }
}